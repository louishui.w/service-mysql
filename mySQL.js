const express = require("express");
const mysql = require('mysql2');
const app = express();
let pool = null;
let port = 3000;

//multer uploadPhoto
const multer = require('multer');
const upload = multer({ dest: 'picture-DB' });
//multer uploadPhoto


const saltRounds = 10;
const secret = "louis";
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { json } = require("express");

const set = new Set();
const getDate = () => {

        let year = new Date().getFullYear();
        let month = new Date().getMonth() + 1;
        let date = new Date().getDate();
        let h = new Date().getHours();
        let am = "AM";
        if (h >= 12) {
                am = "PM"
        }
        let m = new Date().getMinutes();
        let s = new Date().getSeconds();
        return `Date : ${year} / ${month} /${date} Times: ${h}: ${m} : ${s}  ${am}`

}



let gentJWT = (payload) => {
        const options = {
                expiresIn: '100 day',
        }
        return jwt.sign(payload, secret, options);
}

let getRefreshJWT = (payload) => {
        return jwt.sign(payload, secret);
};

app.use(express.json()); //req.body....something
app.use("/picture-DB", express.static('picture-DB'));


app.post('/register', (req, res) => {
        const username = req.body.name;
        const password = req.body.password;

        //if (!username || !password)


        bcrypt.hash(password, saltRounds, async function (err, hash) {
                if (err) {
                        return res.json({
                                success: false
                        });
                }

                try {
                        // add check usernamer duplicate
                        const [result, fields] = await pool.execute(`INSERT INTO member (username, password)VALUES (?,?)`, [username, hash])
                        console.log(result);

                        // res.json(result)
                        if (result.affectedRows === 1) {
                                let token = gentJWT({
                                        username: username
                                })
                                let refreshToken = getRefreshJWT({   
                                        username: username
                                })

                                res.json({
                                        token:  ,
                                        refreshToken: refreshToken
                                });

                        }
                } catch (error) {
                        console.log(error)
                        res.json({ error: true })
                }

        })
})

app.post("/user-login", async (req, res) => {
        const username = req.body.name;
        const password = req.body.password;
        // console.log(username)
        try {

                const [result] = await pool.execute(`select username, password from member where username =? `, [username]);
                console.log(result);
                if (result.length === 0) {


                        res.json({
                                success: false

                        })

                } else {
                        bcrypt.compare(password, result[0].password, function (err, result) {
                                if (err) {

                                        return res.json({
                                                success: false
                                        })
                                }
                                if (result) {

                                        let token = gentJWT({
                                                username: username
                                        })
                                        let refreshToken = getRefreshJWT({
                                                username: username
                                        })
                                        res.json({
                                                token: token,
                                                refreshToken: refreshToken
                                        })
                                } else {
                                        res.json({
                                                success: false
                                        })
                                }
                        })
                }

        } catch (error) {
                console.log(error)
                res.json({ error: true })
        }


});

// getListOfService Or get Name of service

app.get("/services", async (req, res) => {
        let serviceName = req.query.name;

        if (serviceName) {
                try {
                        //if(){

                        // }else{

                        // }
                        const [result] = await pool.execute
                        (`select * from service 
                        where serviceName = ? and idDel = ? `, [serviceName,false]);

                        
                        console.log(result); //log result you can see denormalize obj of path and comments
                        if(result.length !== 0 ){
                                
                                res.json(result);
                        }else{
                                res.json({
                                        success:false,
                                        msg: "service not exist"
                                })
                        }


                } catch (error) {
                        console.log(error);
                        res.json({
                                success: false,
                                msg: "can not try"
                        })
                }

        } else {
                try {
                        const [result] = await pool.execute(`select * from service`)


                        let arr = [result]; //log result you can see denormalize obj of path and comments
                        console.log(arr);

           
                        res.json(result);
                } catch (error) {
                        res.json({
                                success: false
                        })
                }
        }
})



function createObj(acc, currObj) {
        if (acc[currObj.serviceId]) {

                acc[currObj.serviceId].picture.push(currObj.path_id);
                acc[currObj.serviceId].comments.push
                        ({ content: currObj.content, author: currObj.author, date: currObj.date })
        }
        else {
                acc[currObj.serviceId] = currObj;
                acc[currObj.serviceId].picture = [];
                acc[currObj.serviceId].comments = [];
                acc[currObj.serviceId].bookby = "";
                acc[currObj.serviceId].picture.push(currObj.path_id);
                acc[currObj.serviceId].comments.push
                        ({ content: currObj.content, author: currObj.author, date: currObj.date })
                acc[currObj.serviceId].bookby = currObj.book_by;

        }


        return acc;

}

// revise 用function filter 唔洗重複入
// function createObj(acc, currObj) {
//         if (acc[currObj.serviceId]) {

//                 if(acc[currObj.serviceId].picture.indexOf(currObj.path_id)===-1){ // indexOf== -1

//                         acc[currObj.serviceId].picture.push(currObj.path_id);
//                 }
//                 acc[currObj.serviceId].comments.push
//                         ({ content: currObj.content, author: currObj.author, date: currObj.date })
//         }
//         else {
//                 acc[currObj.serviceId] = currObj;
//                 acc[currObj.serviceId].picture = [];
//                 acc[currObj.serviceId].comments = []
//                 acc[currObj.serviceId].picture.push(currObj.path_id);
//                 acc[currObj.serviceId].comments.push
//                         ({ content: currObj.content, author: currObj.author, date: currObj.date })

//         }



//         return acc;

// }


// get detailOfService
app.get("/services/:serviceId", async (req, res) => {
        let serviceId = req.params.serviceId;

        try {

                const [result] = await pool.execute
                        (`select * from service 
                        left join picture on service.id = picture.serviceId
                        left join comments on service.id = comments.serviceId
                        left join booking on service.id = booking.service_id 
                        where service.id= ? `, [serviceId]);
                
                let arr = result; //log result you can see denormalize obj of path and comments

                console.log(arr);
                

                const finalResult = arr.reduce(createObj, {});

                remove(finalResult);
                
               
                function remove(finalResult){

                        let cleaning=Object.values(finalResult)[0];
                        delete cleaning.content;
                        delete cleaning.author;
                        delete cleaning.date;
                        delete cleaning.path_id;
                        delete cleaning.book_by;
                        console.log(cleaning)
                
                        
                }
                //duplicate picture & comments without below code...
                let x = Object.values(finalResult)[0].picture;
                let filtered = x.filter((el, index) => {

                        return x.indexOf(el) === index
                })

                Object.values(finalResult)[0].picture = filtered;

                ///commmnets 
                let y = Object.values(finalResult)[0].comments;
                const commentsArr = [...new Set(y.map(item => JSON.stringify(item)))].map(item => JSON.parse(item));
                Object.values(finalResult)[0].comments = commentsArr;
                // //duplicate picture & comments without above code...
                res.json(finalResult);
        } catch (error) {
                console.log(error);
                res.json({
                        success: false
                })
        }
});

// update content of service
app.patch("/services/:serviceId", auth, async (req, res) => {
        
        let serviceId = req.params.serviceId;
        
        
        let decode = req.tokenPayload;
        let serviceOwner = decode.username;
        let name = req.body.name;
        let description = req.body.description;
        
        
        // if(serviceId !==) input not exist serivceId??
        
        try {

                const [result] = await pool.execute
                (`select * from service where id = ? and serviceOwer=?`, [serviceId, serviceOwner]);
                console.log(result)
                if (result.length === 0) {
                        return res.json({
                                success: false,
                                msg: "not the user"
                        })
                }
        
                
                // console.log(result)
                // console.log(target);
                console.log('here')
                
                let updateQuery = [];
                let preparedStatementParams = [];

                if (name) {
                        updateQuery.push("serviceName=?");
                        preparedStatementParams.push(name);

                }
                if (description) {
                        updateQuery.push("description=?");
                        preparedStatementParams.push(description);
                }

                let query = updateQuery.join(",") //join method array to string , query is string now
                // console.log(query);

                preparedStatementParams.push(parseInt(serviceId));
                console.log(preparedStatementParams);

                console.log(`update service set ${query} where id = ?`)
                // return
                const [result2] = await pool.execute(`update service set ${query} where id = ?`, preparedStatementParams)
                // const [result2] = await pool.execute(`update services set ${serviceName=?} where id = ?`,[preparedStatementParams])

                console.log(result2);

                res.json({
                        success: true

                })

        } catch (error) {
                console.log(error)
                res.json({
                        success: false,
                        msg: "here"

                })
        }
})
function auth(req, res, next) {
        console.log("in auth middleware");
        const authorizationHeader = req.headers.authorization;
        const splittedAuth = authorizationHeader.split(" ");
        
        if (splittedAuth[0] === "Bearer") {
                const token = splittedAuth[1];
                jwt.verify(token, secret, function (err, decoded) {

                        if (err) {
                                res.status(401);
                                return res.json({
                                        success: false
                                });
                        }
                        req.tokenPayload = decoded; //here
                        // console.log(req.tokenPayload);

                        next();
                });
        } else {
                res.status(401);
                return res.json({
                        success: false
                });
        }
}



//create service//create service//create service//create service//create service//create service//create service

app.post("/services", auth, async (req, res) => {
        let decoded = req.tokenPayload;
        let serviceName = req.body.name;
        let description = req.body.description;
        console.log(decoded.username);


        try {
                const [result] = await pool.execute(`insert into service(serviceOwer,serviceName,description,availability,idDel,likeCount) values(?,?,?,?,?,?)`,
                        [decoded.username, serviceName, description, true, false, 0])
                console.log(result);

                res.json({
                        success: true
                })

        } catch (error) {
                console.log(error)
                res.json({ error: true })
        }

})

app.post('/services/:serviceId/picture', auth, upload.single('picture'), async (req, res) => {
        console.log(req.file);
        let serviceId = req.params.serviceId;
        let decoded = req.tokenPayload;
        let username = decoded.username;
        let picture = req.file.path;
        console.log(picture);
        console.log(username);
        console.log(serviceId);
        try {
                const [result1] = await pool.execute(`select id from service where serviceOwer = ? and id=?`, [username, serviceId])
                if (result1.length === 0) {
                        return res.json({
                                success: false,
                                msg: 'not the user'
                        })
                }
                const [result] = await pool.execute(`insert into picture(path_id,serviceId) values(?,?)`, [picture, serviceId])

                console.log(result);
                res.json({
                        success: true,
                        "imgName": req.file.filename,
                        "imgPath": req.file.path
                })
        } catch (error) {
                console.log(error)
                return res.json({
                        success: false
                })
        }


});

app.post("/services/:serviceId/comments", auth, async (req, res) => {
        let content = req.body.content;

        let decoded = req.tokenPayload;

        let author = decoded.username

        let serviceId = req.params.serviceId;

        let date = getDate();


        try {
                const [result1] = await pool.execute(`select * from service where id=? `, [serviceId]);
                console.log(result1);
                if (result1.length === 0) {

                        return res.json({
                                success: false,
                                msg: `not found service`
                        })
                }
                const [result] = await pool.execute(`insert into comments(serviceId, content,author,date) value(?,?,?,?)`, [serviceId, content, author, date]);

                console.log(result);

                res.json({
                        success: true,

                        msg: ' comment OK!'
                })
        } catch (error) {
                // console.log(error);
                return res.json({
                        success: false
                })
        }

})

app.delete("/services/:serviceId", auth, async (req, res) => {
        let decoded = req.tokenPayload;
        let serviceOwner = decoded.username;
        let serviceId = req.params.serviceId;
        try {
                const [result] = await pool.execute(`select * from service where serviceOwer=? and id = ?`, [serviceOwner, serviceId])
                if (result.length === 0) {
                        return res.json({
                                success: false,
                                msg: 'you are not serviceOwner'
                        })
                }
                //just update idDel false = 1 , not really delete

                
                await pool.execute(`update service set idDel=? where id=?`, [1, serviceId]);
                if(affectedRows ===1){

                        res.json({
                                success: true
                        })
                }else{
                        res.json({
                                success: false
                        })
                }

        } catch (error) {
                console.log(error)
                res.json({
                        success: false
                })
        }
})


//bookBy
app.patch('/services/:serviceId/bookby', auth, async (req, res) => {
        let serviceId = req.params.serviceId;
        let username = req.tokenPayload.username;
        console.log(username)
        try {
                //  availability ===true 先可book 
                const [result] = await pool.execute(`select * from service where id= ? and availability=?`, [serviceId, true])
                console.log(result);
                

                if (result[0].serviceOwer === username) {
                        //check 自己book 唔到自己service

                        res.json({
                                success: false,
                                msg : "you are serviceOwner can not book!"
                        })

                } else {
                        //book 完 availabity === false 再book 唔到


                        const [result]= await pool.execute(`insert into booking (book_by,service_id,isDelBooking)values(?,?,?)`, [username, serviceId,false]);
                        const [result2] = await pool.execute(`update service set availability=? where id= ?`, [false, serviceId])
                        res.json({
                                success: true
                        })
                }
        } catch (error) {

                console.log(error)
                res.json({
                        success: false,
                        msg : "service is booked"
                })
        }
})
//delete bookby


app.delete('/services/:serviceId/bookby', auth, async (req, res) => {
        let serviceOwner = req.tokenPayload.username;
        let serviceId = req.params.serviceId;
        console.log(serviceOwner)
        try {
                // const [result] = await pool.execute
                //         (`select * from service  
                // left join booking 
                // on service.id = booking.service_id 
                // where service.id =? 
                // and service.serviceOwer = ? 
                // and booking.isDelBooking = ?`, [serviceId, serviceOwner, false]);
                const [result2] =await pool.execute
                (`select * from service where id=? and serviceOwer=?`,[serviceId,serviceOwner])
                if (result2.length === 0) {
                        return res.json({
                                success: false,
                                msg : "your are not serviceowner"
                        })
                }
            
                // console.log(result2);
                const [result1] = await pool.execute
                        (`update booking set isDelbooking = ? where service_id = ?`, [true, serviceId])
                res.json({
                        success: true
                })
        } catch (error) {
                console.log(error)
                res.json({
                        success: false
                })
        }

});
// likeCount
app.patch('/services/:serviceId/like', auth, async (req, res) => {
        let serviceId = req.params.serviceId;
        let username = req.tokenPayload.username;
        try {

                const [result] = await pool.execute(`update service set likeCount = likeCount +1 where id= ?`, [serviceId]);
                console.log(result);
                if (result.affectedRows === 0) {
                        return res.json({
                                success: false
                        })

                }
                res.json({
                        success: true
                })
        } catch (error) {
                console.log(error)
                res.json({
                        success: false
                })
        }


})





app.all('*', (req, res) => res.json('That route does not exist!'))


app.listen(port, () => {
        console.log("Example app listening on port !" + port);
        pool = mysql.createPool({
                host: 'localhost',
                port: 3306,
                user: 'root',
                password: 'root',
                database: 'service_app'
        }).promise();
});




// app.get("/services", async(req,res)=>{
//         let serviceName =req.query.name;
//         let serviceOwner= req.query.serviceOwner;
//         if(serviceName){
//                 try {
//                         const [result] = await pool.execute(`select * from service where serviceOwer= ?` ,[serviceOwner]);
//                         console.log(result)
//                         console.log('here');
//                         res.json(result)
//                         //path of photo?????
//                 } catch (error) {
//                         res.json({
//                                 success:false
//                         })
//                 }

//         }else{
//                 try {
//                         const result = await pool.execute(`select * from service`)

//                         res.json(result[0]);
//                 } catch (error) {
//                         res.json({
//                                 success:false
//                         })
//                 }
//         }
// })